#ifdef	__cplusplus
extern "C" {
#endif

#include "main.h"
#include "UART.h"
#include "ADC.h"
#include <avr/interrupt.h>

#if HoldTime > 3407
error("Hold time should be not longer than 3407ms")
#endif

#define HoldCount (uint16_t)((HoldTime * 1000ul + 13 * 4 / 2) / (13 * 4))

volatile Flags_t Flags = { 0 };

void setup() {
  __asm__ __volatile__("wdr");
  WDTCSR = 0x00;  //Watchdog timeout 2048/128kHz
  DDRB |= 1 << DDB5; //Enable LED output (Arduino Nano D13)
  DDRD &= ~(1 << DDD2); //Disable current sink into Trigger output (Arduino D2) untill triggered
  PORTB = 0x00;  //Disable input pullups
  PORTC = 0x00;  //
  PORTD = 0x00;  //
  PINB = 0x00;   //Set outputs low
  PINC = 0x00;   //
  PIND = 0x00;   //
  CLKPR = 0x00;  //Run at full speed (don't change)
  SMCR = 0x00;   //Idle in sleep mode (not used)
  MCUCR = 0x00;  //Do not use BOR in sleep, do not use Boot Reset Address
  UARTconfigure(); //Configute debug UART
  ADCconfigure(); //Configure the ADC
  sei();  //Enable interrupts
  ADCstart();
}

void loop() {
  static uint16_t HoldTimer = 0;  //Timer variable for longer touch indication
  if (Flags.Touch) {
    HoldTimer = 1;
    Flags.Touch = 0;
  }
  if (Flags.ADCdata) {
    __asm__ __volatile__("wdr"); //Reset watch dog timer only when ADC is running
    uint8_t DebugVal = FilterOut[2]; //Take signle channel (0..3) value for UART output
    Flags.ADCdata = 0;  //Volatile data copied (clear lag detection cause)
    SendByte(DebugVal); //Send raw filter output byte (one channel of 4) to UART
    Flags.LagDetected = 0; //Clear main loop lag detector
    if (HoldTimer) { //Check is trigger output on hold?
      if (HoldTimer++ > HoldCount) { //Is prolongation timed out?
        TriggerOff();
        HoldTimer = 0;
      }
    }
  }
}

#ifdef	__cplusplus
}
#endif
