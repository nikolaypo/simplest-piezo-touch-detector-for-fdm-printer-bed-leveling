#ifdef __cplusplus
extern "C" {
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include "ADC.h"
#include "main.h"

void ADCconfigure(void) {
  DDRC &= ~((1 << DDC3) | (1 << DDC2) | (1 << DDC1) | (1 << DDC0));                                  //Make pins ADC3:ADC0 an inputs.
  DIDR0 = (1 << ADC0D) | (1 << ADC1D) | (1 << ADC2D) | (1 << ADC3D);                                 //Disable digital input buffers at ADC0..ADC3 pins.
  ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (0 << MUX0);                                  //1.1V reference, left justified (only 8 bits used). MUX on ADC0.
  ADCSRB = (0 << ACME) | (0b000 << ADTS0);                                                           //ACME disabled (not used). ADTS free running (autosampling).
  ADCSRA = (0 << ADEN) | (0 << ADSC) | (1 << ADATE) | (1 << ADIF) | (1 << ADIE) | (0b100 << ADPS0);  //Maximum ADC sampling speed. Auto trigger enabled.
  ADCSRA |= 1 << ADEN;                                                                               //Turn on the ADC
}

void ADCstart(void) {
  ADCSRA |= 1 << ADSC;  //Start first conversion of free-running mode
}

volatile uint8_t Channel = 0;  //Channel scan index
volatile uint8_t Memory[FilterSize][4] = { //Filter memory
  { 0 }
};
volatile uint8_t Idx = 0;               //Oldest memory sample pointer
volatile uint8_t FilterOut[4] = { 0 };  //Channel filter output values

ISR(ADC_vect) {  //ADC interrupt routine
  uint8_t New = 0;
  switch (Channel) {  //Choose channel for next sample
    case 0:           //Channel 0 just sampled
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (2 << MUX0);
      if (ADCH < BiasThreshold) {  //Regulate bias on ADC channel 0
        PORTC |= 1 << PORTC0;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC0);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
      } else {
        New = 0; //None bias feededng required, count sample as "0"
      }
      break;
    case 1:  //Channel 1 just sampled
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (3 << MUX0);
      if (ADCH < BiasThreshold) {  //Regulate bias on ADC channel 1
        PORTC |= 1 << PORTC1;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC1);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
      } else {
        New = 0; //None bias feededng required, count sample as "0"
      }
      break;
    case 2:  //Channel 2 just sampled
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (0 << MUX0);
      if (ADCH < BiasThreshold) {  //Regulate bias on ADC channel 2
        PORTC |= 1 << PORTC2;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC2);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
      } else {
        New = 0; //None bias feededng required, count sample as "0"
      }
      break;
    case 3:  //Channel 3 just sampled
      //Choose input for after next conversion
      ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (1 << MUX0);
      if (ADCH < BiasThreshold) {  //Regulate bias on ADC channel 3
        PORTC |= 1 << PORTC3;      //Turn on weak pull-up
        PORTC &= ~(1 << PORTC3);   //Turn off weak pull-up
        New = 1; //If channel bias feedeng occured, count sample as "1"
      } else {
        New = 0; //None bias feededng required, count sample as "0"
      }
      break;
    default:
      while (1)
        ;  //Wait for watchdog reset
      break;
  }
  FilterOut[Channel] += New;                   //Add new sample to filter sum
  FilterOut[Channel] -= Memory[Idx][Channel];  //Remove oldest sample from sum
  if (FilterOut[Channel] > TriggerThreshold) { //Check for filter output threshold reaching
    TriggerOn(); //Threshold is reached. Activate touch detector output (turn on current sink)
    Flags.Touch = 1; //Indicate the triggering for main loop signal prolongation timer
  }
  Memory[Idx][Channel] = New;  //Replace oldest value in memory by newest one
  if (++Channel >= 4) { //Check was last channel converted?
    Channel = 0; //Continue sampling in a loop channel
    if (++Idx >= FilterSize) { //Go forward and make filter memory circular
      Idx = 0; //Reset memory index
    }
    if (Flags.ADCdata) { //Check was debug data taken by main loop in time?
      Flags.LagDetected = 1; //If not, set Lag flag for indication and investigation
    }
    Flags.ADCdata = 1; //Indicate all 4 channel filtered samples ready
  }
}

#ifdef __cplusplus
}
#endif
